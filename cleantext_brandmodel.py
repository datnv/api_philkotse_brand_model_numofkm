import numpy as np
import re

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  text = re.sub(cleanr, ' ', raw_html)
  return text

def split_to_words(text):
    return text.split()

def convert_title_and_description_to_words(tilte,description):
    text = tilte+' '+description
    text = cleanhtml(text).lower()
    text = re.sub('[^0-9a-zA-Z]+', ' ', text)
    return split_to_words(text)

#text="toi la nguyen Van-Toi \n dat."
#print(text.lower())