from flask import Flask, render_template, flash, request
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from predict_numofkm import Numofkm
from predict_brand_model import BrandModel
import time

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'


class ReusableForm(Form):
    title = TextAreaField('Title: ', validators=[validators.required()])
    description = TextAreaField('Description: ', validators=[validators.required()])


brandmodel = BrandModel()
numofkm = Numofkm()


@app.route("/", methods=['GET', 'POST'])
def hello():
    form = ReusableForm(request.form)

    print (form.errors)
    if request.method == 'POST':
        start = time.time()
        #brandmodel = BrandModel()
        #numofkm = Numofkm()
        title = request.form['title']
        description = request.form['description']
        print(title)
        print(description)
        #if '\n' in description:
        #    print('there are new line symbol in description')
        #for c in description:
        #    if c=='\n':
        #        print('there are new line symbol in description')
        measure = numofkm.predict(title, description)
        brand, model = brandmodel.predict(title, description)
        if brand == None:
            print("Brand: Unknow")
            s1="Brand: Unknow"
        else:
            print("Brand: " + str(brand))
            s1="Brand: " + str(brand)

        if model == None:
            print("Model: Unknow")
            s2="Model: Unknow"
        else:
            print("Model: " + str(model))
            s2="Model: " + str(model)

        if measure == None:
            print('Num of km: Unknow')
            s3='Num of km: Unknow'
        else:
            print('Num of km: ' + str(measure))
            s3='Num of km: ' + str(measure)
        if form.validate():
            # Save the comment here.
            flash(s1)
            flash(s2)
            flash(s3)
        else:
            flash('All the form fields are required. ')
        end = time.time()
        flash('Processing time: '+str(round(end-start,4))+' seconds')

    return render_template('hello.html', form=form)


if __name__ == "__main__":
    #app.run(processes=100,host='0.0.0.0',use_reloader=False)
    app.run(host='0.0.0.0',use_reloader=False)
