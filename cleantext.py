import numpy as np
import re
import math
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.externals import joblib
import os
from scipy.sparse import csr_matrix
from save_and_load_dictionary import save
from save_and_load_dictionary import load


def split_to_sentences(title, description):
    sentence_year_pattern = re.compile("\d[\s]+years-|\d[\s]+year-")
    sentence_downpayment_pattern = re.compile("down payment|all in")
    sentence_price_pattern = re.compile("price")
    sentence_specialkm_pattern = re.compile("km/")
    raw_html = title + '\n' + description
    list_phrases = re.split('[\n]|<.*?>|\.\s|\,\s|\;\s|\!\s|\t', raw_html)
    for i in range(len(list_phrases)):
        list_phrases[i] = list_phrases[i].lower()

        if bool(sentence_year_pattern.search(list_phrases[i])) or bool(
                sentence_downpayment_pattern.search(list_phrases[i])) or bool(
                sentence_price_pattern.search(list_phrases[i])) or bool(sentence_specialkm_pattern.search(list_phrases[i])) :
            list_phrases[i] = 'aaaaaaaaaa'
        else:
            list_phrases[i] = list_phrases[i].replace('+++', '000')
            list_phrases[i] = list_phrases[i].replace('***', '000')
            list_phrases[i] = re.sub('[^0-9a-zA-Z\s\,\.\$\%\₱]+', ' ', list_phrases[i])
            #list_phrases[i] = re.sub('[^0-9a-zA-Z\s\,\.]+', my_replace, list_phrases[i])
    return list_phrases

def get_type_of_measure(gram):
    if ('%' in gram) or ('$' in gram) or ('₱' in gram) or ('mph' in gram) or ('peso' in gram):
        type_measure = 4
    elif bool(re.compile('km[\s]*h').search(gram)) or bool(re.compile('km[\s]*l').search(gram)) or bool(
            re.compile('km[\s]*liter').search(gram)) or bool(
            re.compile('km[\s]*hr').search(gram)) or bool(
            re.compile('g[\s]*km').search(gram)) :
        type_measure = 4
    elif 'km' in gram or 'kms' in gram or 'klm' in gram or 'miles' in gram:
        type_measure = 0
    elif 'k' in gram or 't' in gram:
        type_measure = 1
    elif ',' in gram:
        type_measure = 2
    else:
        type_measure = 3
    return type_measure


def get_measure_from_string(gram, sentence_have_mileage, get_complete):
    if gram[0] == '.' or gram[0] == ',':
        gram = gram[1:]
    measure = None
    #if ('%' in gram) or ('$' in gram) or ('₱' in gram) or ('g' in gram) :
    #    for i in range(len(gram)):
    #        c = gram[i]
    #        if c == '%' or c=='$' or c=='₱' or c=='g':
    #            measure=float((re.sub('\,', '', gram[0:i])))
    for j in range(len(gram)):
        c = gram[j]
        if (c=='%') or (c=='$') or (c=='₱') or (c=='g') or (c=='m') or (c=='p'):
            measure = float((re.sub('\,', '', gram[0:j])))
            break
        elif c.isalpha():
            if c == 't':
                measure = float(re.sub('\,', '', gram[0:j])) * 1000
            elif c == 'k':
                if j + 1 >= len(gram):
                    measure = float(re.sub('\,', '', gram[0:j])) * 1000
                elif gram[j + 1] == 'm' or gram[j + 1] == 'l':
                    measure = float(re.sub('\,', '', gram[0:j]))
                else:
                    measure = float(re.sub('\,', '', gram[0:j])) * 1000
            break
        # if c.isdigit() and j==len(gram)-1:
        if j == len(gram) - 1:
            if gram[j] == '.':
                measure = float(re.sub('\,', '', gram[0:len(gram) - 1]))
            else:
                measure = float(re.sub('\,', '', gram))
    # print(gram)
    # if measure <= 500:
    #    measure *= 1000

    if 'miles' in gram:
        measure = measure*1.609344

    measure = int(measure)
    sentence_have_mileage = 0 if sentence_have_mileage else 1
    if get_complete:
        type_measure = get_type_of_measure(gram)
        measure = [measure, sentence_have_mileage, type_measure]
    return measure


def check_measure_correct(numofkm_prediction, numofkm_label):
    num_of_digit = len(str(numofkm_prediction))
    x = int(math.pow(10, (num_of_digit - 1)))
    m = numofkm_prediction // x
    a = (m + 1) * x
    b = (m - 1) * x if m > 1 else m * x
    if numofkm_label >= b and numofkm_label <= a:
        return True
    else:
        return False


'''
def transform_data_to_tfvector(list_content,is_training):
    if is_training:
        print('delete old model')
        for file in os.listdir('model'):
            os.remove('model/' + file)
        print('done')
        count_vect = CountVectorizer()
        X_train_counts = count_vect.fit_transform(list_content)
        print('feature names: ')
        #print(count_vect.get_feature_names())
        tf_transformer = TfidfTransformer(use_idf=False).fit(X_train_counts)
        X_tf = tf_transformer.transform(X_train_counts)
        print('save count vector and tf transformer ...')
        joblib.dump(count_vect, 'model/count_vect.pkl')
        joblib.dump(tf_transformer, 'model/tf_transformer.pkl')
        print('done')

    else:
        print('load model ....')
        lin_clf = joblib.load('model/lin_clf.pkl')
        count_vect = joblib.load('model/count_vect.pkl')
        tf_transformer = joblib.load('model/tf_transformer.pkl')
        print('done')
        X_valid_counts = count_vect.transform(list_content)
        X_tf = tf_transformer.transform(X_valid_counts)

    return X_tf
'''
def transform_data_to_vectorfeature(list_data, kshingle, radius, is_training):
    if is_training:
        print('delete old model')
        for file in os.listdir('model'):
            os.remove('model/' + file)
        print('done')
        # FEATURE_MATRIX =
        print('build feature dictionary')
        dic_feature_to_index = {}
        dic_feature_to_index['type_measure_0'] = 0
        dic_feature_to_index['type_measure_1'] = 1
        dic_feature_to_index['type_measure_2'] = 2
        dic_feature_to_index['type_measure_3'] = 3
        dic_feature_to_index['type_measure_4'] = 4
        index = 5
        for i in range(len(list_data)):
            data = list_data[i]
            # print(data)
            type_measure = int(data[0])
            previous_string = '__'+''.join(data[1])
            after_string = ''.join(data[2])+'__'
            nb_previous = 1
            for j in range(len(previous_string) - 1, kshingle - 2, -1):
                shingle = previous_string[j + 1 - kshingle:j + 1]
                key = 'p' + str(nb_previous) + '_' + str(shingle)
                #key = 'p' + '_' + str(shingle)
                if key not in dic_feature_to_index:
                    dic_feature_to_index[key] = index
                    index += 1
                nb_previous += 1
                if nb_previous == radius:
                    break

            nb_after = 1
            for j in range(len(after_string) - kshingle + 1):
                shingle = after_string[j:j + kshingle]
                key = 'a' + str(nb_after) + '_' + str(shingle)
                #key = 'a' + '_' + str(shingle)
                if key not in dic_feature_to_index:
                    dic_feature_to_index[key] = index
                    index += 1
                nb_after += 1
                if nb_after == radius:
                    break
            # print('===================')
        print('the number of features is: ' + str(len(dic_feature_to_index.keys())))
        print('save feature dictionary')
        save(dic_feature_to_index, 'model/dic_feature_to_index.npy')
        print('convert data to vector features')
        SPRASE_FEATURE_MATRIX = convert_data_to_sparse_matrix(list_data, kshingle, radius, dic_feature_to_index)
        return SPRASE_FEATURE_MATRIX
    else:  # if in validation
        dic_feature_to_index = load('model/dic_feature_to_index.npy')
        SPRASE_FEATURE_MATRIX = convert_data_to_sparse_matrix(list_data, kshingle, radius, dic_feature_to_index)
        return SPRASE_FEATURE_MATRIX

def convert_data_to_sparse_matrix(list_data, kshingle, radius, dic_feature_to_index):
    row = []
    col = []
    list_feature_value = []
    for i in range(len(list_data)):
        data = list_data[i]
        type_measure = int(data[0])
        previous_string = '__'+''.join(data[1])
        after_string = ''.join(data[2])+'__'
        nb_previous = 1
        row.append(i)
        col.append(dic_feature_to_index['type_measure_' + str(type_measure)])
        list_feature_value.append(1)
        for j in range(len(previous_string) - 1, kshingle - 2, -1):
            shingle = previous_string[j + 1 - kshingle:j + 1]
            key = 'p' + str(nb_previous) + '_' + str(shingle)
            #key = 'p' + '_' + str(shingle)
            if key in dic_feature_to_index:
                row.append(i)
                col.append(dic_feature_to_index[key])
                list_feature_value.append(1)

            nb_previous += 1
            if nb_previous == radius:
                break
        nb_after = 1
        for j in range(len(after_string) - kshingle + 1):
            shingle = after_string[j:j + kshingle]
            key = 'a' + str(nb_after) + '_' + str(shingle)
            #key = 'a' + '_' + str(shingle)
            if key in dic_feature_to_index:
                row.append(i)
                col.append(dic_feature_to_index[key])
                list_feature_value.append(1)

            nb_after += 1
            if nb_after == radius:
                break
    SPRASE_FEATURE_MATRIX = csr_matrix((list_feature_value, (row, col)),
                                       shape=(len(list_data), len(dic_feature_to_index.keys())))
    return SPRASE_FEATURE_MATRIX
