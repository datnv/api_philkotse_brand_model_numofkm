import numpy as np

def save(dic,path):
    #print('save dic to '+path)
    list=[]
    for key in dic.keys():
        list.append([key,dic[key]])
    #list = np.array(list)
    np.save(path, np.array(list,dtype=object))
    #print(type(list[0][0]))
    #print(type(list[0][1]))
    #print('done')

def load(path):
    print('load dic in '+path)
    list=np.load(path)
    #print(type(list[0][0]))
    #print(type(list[0][1]))
    dic={}
    for i in range(len(list)):
        dic[list[i][0]]=list[i][1]
    return dic