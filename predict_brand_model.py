from save_and_load_dictionary import load
import numpy as np
from cleantext_brandmodel import convert_title_and_description_to_words


class BrandModel(object):
    def __init__(self):
        print('load model brand and model .......')
        self.dic_modelname_modelid = load('file npy/dic_modelname_modelid.npy')
        #self.dic_productid_modelid = load('file npy/dic_productid_modelid.npy')
        self.dic_modelid_modelnameoriginal = load('file npy/dic_modelid_modelnameoriginal.npy')
        self.dic_brandname_brandid = load('file npy/dic_brandname_brandid.npy')
        #self.dic_productid_brandid = load('file npy/dic_productid_brandid.npy')
        # dic_brandid_brandname = load('file npy/dic_brandid_brandname.npy')
        self.dic_brandid_brandnameoriginal = load('file npy/dic_brandid_brandnameoriginal.npy')
        self.dic_modelid_brandid = load('file npy/dic_modelid_brandid.npy')
        self.dic_brandid_modelnamelist = load('file npy/dic_brandid_modelnamelist.npy')
        self.dic_modelname_duplicate = load('file npy/dic_modelname_duplicate.npy')
        self.dic_brandname_duplicate = load('file npy/dic_brandname_duplicate.npy')
        print('done')


    def predict(self,title,description):
        list_brand_results, list_model_results, list_words = \
            self.brandmodel_classifier(title, description, self.dic_brandname_brandid,
                                  self.dic_modelname_modelid, self.dic_modelid_brandid,
                                  self.dic_brandid_modelnamelist, self.dic_brandname_duplicate, self.dic_modelname_duplicate, 1)
        brandname_predict = None
        modelname_predict = None
        if len(list_brand_results) != 0:
            brandname_predict = self.dic_brandid_brandnameoriginal[list_brand_results[0]]
        if len(list_model_results) != 0:
            modelname_predict = self.dic_modelid_modelnameoriginal[list_model_results[0]]
        return brandname_predict,modelname_predict


    def brandmodel_classifier(self,title, description, dic_brandname_brandid, dic_modelname_modelid, dic_modelid_brandid,
                              dic_brandid_modelnamelist, dic_brandname_duplicate, dic_modelname_duplicate, threso_leven):
        list_words = convert_title_and_description_to_words(title, description)
        list_brand_results = []
        list_model_results = []
        list_brand_positions = []
        list_model_positions = []
        list_gram = []
        list_gram_positions = []
        # build list gram

        for i in range(len(list_words)):
            for n in (3, 2, 1):
                gram = []
                if i + n - 1 <= len(list_words) - 1:
                    for j in range(n):
                        gram.append(list_words[i + j])
                    gram = ''.join(gram)
                    list_gram.append(gram)
                    list_gram_positions.append(i)

        # check if any gram is brandname
        for i in range(len(list_gram)):
            gram = list_gram[i]
            if gram in dic_brandname_brandid.keys():
                if gram not in dic_brandname_duplicate:
                    list_brand_results.append(dic_brandname_brandid[gram])
                    list_brand_positions.append(list_gram_positions[i])
        cannot_predict_model = False
        if len(list_brand_results) != 0:  # if brandname can be predicted
            sorted_indexes = np.argsort(list_brand_positions)
            list_brand_results = np.array(list_brand_results)[sorted_indexes].tolist()
            # model must be in brand
            list_brand_of_model = []
            for i in range(len(list_brand_results)):
                set_candidate_modelname = dic_brandid_modelnamelist[list_brand_results[i]]
                #print(list_brand_results[i])
                #print(set_candidate_modelname)
                for j in range(len(list_gram)):
                    gram = list_gram[j]
                    if gram in set_candidate_modelname:
                        list_model_results.append(
                            dic_modelname_modelid[gram])  # this modelid can be wrong but modelname is still correct
                        list_model_positions.append(list_gram_positions[j])
                        list_brand_of_model.append(i)

            if len(list_model_results) != 0:  # if modelname can be predicted
                sorted_indexes = np.argsort(list_brand_of_model)
                list_model_results = np.array(list_model_results)[sorted_indexes].tolist()
                list_brand_results[0] = list_brand_results[list_brand_of_model[sorted_indexes[0]]]
                # print(dic_brandid_brandname[list_brand_results[0]]+'    '+dic_modelid_modelname[list_model_results[0]])
            else:
                cannot_predict_model = True

        if len(list_brand_results) == 0 or cannot_predict_model == True:  # if brandname can not be predicted or model cannot predict
            for i in range(len(list_gram)):
                gram = list_gram[i]
                if gram in dic_modelname_modelid.keys():
                    if self.check_model_trusted(gram, dic_modelname_duplicate):
                        list_model_results.append(dic_modelname_modelid[gram])
                        list_model_positions.append(list_gram_positions[i])
            if len(list_model_results) != 0:  # if modelname can be predicted
                sorted_indexes = np.argsort(list_model_positions)
                list_model_results = np.array(list_model_results)[sorted_indexes].tolist()
                # infrence brand based on model
                list_brand_results.append(dic_modelid_brandid[list_model_results[0]])

        return list_brand_results, list_model_results, list_words

    def check_model_trusted(self,gram,dic_modelname_duplicate):
        if gram in dic_modelname_duplicate or gram.isdigit() or len(gram)<=5:
            return False
        else:
            return True
