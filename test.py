from predict_numofkm import Numofkm
from predict_brand_model import BrandModel
numofkm = Numofkm()

title = '2013 Hyundai Accent Hatchback 1.6 CRDi MT'

descrition = "<div>2013 Hyundai Accent Hatchback 1.6 CRDi<br /> Manual Transmission<br /> 44k mileage only</div>"

measure = numofkm.predict(title,descrition)
brandmodel = BrandModel()

brand, model = brandmodel.predict(title,descrition)

if brand == None:
    print("Brand: Unknow")
else:
    print("Brand: "+str(brand))

if model == None:
    print("Model: Unknow")
else:
    print("Model: "+str(model))

if measure == None:
    print('Num of km: Unknow')
else:
    print('Num of km: '+str(measure))
