from flask import Flask, render_template, flash, request
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from predict_numofkm import Numofkm
from predict_brand_model import BrandModel
import time

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'

numofkm = Numofkm()
brandmodel = BrandModel()
print("sdsdsdsdsdsdsdsdsdsdsd")


class ReusableForm(Form):
    title = TextAreaField('Title: ', validators=[validators.required()])
    description = TextAreaField('Description: ', validators=[validators.required()])




@app.route("/", methods=['GET', 'POST'])
def hello():
    form = ReusableForm(request.form)

    print (form.errors)
    if request.method == 'POST':
        start = time.time()
        #brandmodel = BrandModel()
        #numofkm = Numofkm()
        title = request.form['title']
        description = request.form['description']
        brand_name=""
        model_name = ""
        num_of_km=""
        print(title)
        print(description)
        #if '\n' in description:
        #    print('there are new line symbol in description')
        #for c in description:
        #    if c=='\n':
        #        print('there are new line symbol in description')
        measure = numofkm.predict(title, description)
        brand, model = brandmodel.predict(title, description)
        if brand == None:
            print("Brand: Unknow")
            brand_name="unknown"
        else:
            print("Brand: " + str(brand))
            brand_name=str(brand)

        if model == None:
            print("Model: Unknow")
            model_name="unknown"
        else:
            print("Model: " + str(model))
            model_name=str(model)

        if measure == None:
            print('Num of km: Unknow')
            num_of_km="unknown"
        else:
            print('Num of km: ' + str(measure))
            num_of_km=str(measure)
        end = time.time()
        print('Processing time: '+str(round(end-start,4))+' seconds')
    return '{"brand":"'+brand_name+'","model":"'+model_name+'","mileage":"'+num_of_km+'"}'


if __name__ == "__main__":
    app.run(host='0.0.0.0',use_reloader=False)
