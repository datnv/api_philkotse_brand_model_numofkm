import numpy as np
from cleantext import split_to_sentences
import re
from cleantext import get_measure_from_string
from cleantext import check_measure_correct
from cleantext import get_type_of_measure
from cleantext import convert_data_to_sparse_matrix
from sklearn.externals import joblib
from save_and_load_dictionary import load

class Numofkm(object):
    def __init__(self):
        self.pattern = re.compile("[\,\.]?[0-9]+[\,\.]?[0-9]*[\s]*([tk]|th|tho|thou|thousand)?[\s]*"
                             "(km|kms|klm|miles|[$]|[₱]|[%]|pesos|peso|(km[\s]*h)|(km[\s]*l)"
                             "|(km[\s]*liter)|(km[\s]*hr)|g[\s]*km|mph)?[\,\.]?\\Z")
        self.year_pattern = re.compile("[\,\.]?(200[1-9]|201[0-9]|19[1-9][0-9])[\,\.]?\\Z")
        self.thresof_proba = 0.94
        self.kshingle = 3
        self.radius = 15
        print('load model numofkm....')
        self.lin_clf = joblib.load('model/lin_clf.pkl')
        self.dic_feature_to_index = load('model/dic_feature_to_index.npy')
        print('done')

    def predict(self,title,description):
        list_data = []  # list data
        measure_of_post = None
        list_sentences = split_to_sentences(title, description)
        max_proba = 0
        for sentence in list_sentences:
            list_words = sentence.split()
            sentence_have_mileage = False
            if 'mileage' in sentence:
                sentence_have_mileage = True
            for i in range(len(list_words)):
                for n in (3, 2, 1):
                    gram = []
                    if i + n - 1 <= len(list_words) - 1:
                        for j in range(n):
                            gram.append(list_words[i + j])
                        gram = ' '.join(gram)
                        if bool(self.pattern.match(gram)) and not bool(self.year_pattern.match(gram)):
                            measure = get_measure_from_string(gram, sentence_have_mileage, get_complete=False)
                            list_words_previous = [list_words[j] for j in range(i)]
                            list_words_after = [list_words[j] for j in range(i + n, len(list_words))]
                            type_measure = get_type_of_measure(gram)
                            if type_measure == 4:
                                break
                            sublist = [type_measure, list_words_previous, list_words_after]
                            sparse_features = convert_data_to_sparse_matrix(np.array([sublist], dtype=object), self.kshingle, self.radius,
                                                                            self.dic_feature_to_index)
                            # list_data.append(sublist)
                            label_predict = self.lin_clf.predict(sparse_features)[0]
                            proba_predict = max(self.lin_clf.predict_proba(sparse_features)[0])
                            if label_predict == 0 and proba_predict >= self.thresof_proba and proba_predict > max_proba:
                                measure_of_post = measure
                                max_proba = proba_predict
                            # score_predict=math.fabs(lin_clf.decision_function(sparse_features)[0])
                            # if label_predict == 0 and  score_predict > max_score:
                            #    print(score_predict)
                            #    measure_of_post = measure
                            #    max_score = score_predict
                            break

        if measure_of_post != None:
            #if measure_of_post >= 100 and measure_of_post <= 400000:
            return measure_of_post
        else:
            return None

